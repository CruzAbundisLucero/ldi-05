/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package lenguajes.de.intefaz;

/**
 *
 * @author Lucero
 */
 
import java.util.Scanner; 

public class Hexadecimal { 

public static long Hex_Decimal(String n) 
{ 
StringBuilder conversion = new StringBuilder(n).reverse(); 
long suma=0; 
for( int i = 0; i < conversion.length(); i++) 
{ 
if(conversion.charAt(i)=='A') 
    suma = suma + 10 * (long)Math.pow(16, i); 
else if(conversion.charAt(i)=='B') 
    suma = suma + 11 * (long)Math.pow(16, i); 
else if(conversion.charAt(i)=='C') 
    suma = suma + 12 * (long)Math.pow(16, i); 
else if(conversion.charAt(i)=='D') 
    suma = suma + 13 * (long)Math.pow(16, i); 
else if(conversion.charAt(i)=='E') 
    suma = suma + 14 * (long)Math.pow(16, i); 
else if(conversion.charAt(i)=='F') 
    suma = suma + 15 * (long)Math.pow(16, i);	
else 
{ 
    suma = suma + Integer. parseInt("" + conversion. charAt(i)) * (long) Math. pow(16, i) ; 
} 
} 
return suma; 
} 
public static void main(String[]args) 
{ 
Scanner lee = new Scanner(System.in); 
System.out.println("Ingresa un numero hexadecimal"); 
String numero = lee.nextLine().toUpperCase(); 
System.out.println( Hexadecimal. Hex_Decimal(numero) ) ; 
} 
}

/*
String texto = "", textoTemp = ""; 
double num = 0; 

DecimalFormat df = new DecimalFormat("#########.00"); 

public String hexa_decimal(){ 
if(texto.equals("")){ 
return "0"; 
}else{ 
int j = 1; 
if(texto.equals("")){return "0";} 
else{ 
textoTemp = texto.substring(texto.length()-1); 
if(textoTemp.equals("A")){textoTemp = "10";} 
if(textoTemp.equals("B")){textoTemp = "11";} 
if(textoTemp.equals("C")){textoTemp = "12";} 
if(textoTemp.equals("D")){textoTemp = "13";} 
if(textoTemp.equals("E")){textoTemp = "14";} 
if(textoTemp.equals("F")){textoTemp = "15";} 
num += Double.parseDouble(textoTemp) * Math.pow(16,0); 
} 
for(int i=texto.length()-2;i>=0;i--){ 
textoTemp = texto.substring(i,i+1); 
if(textoTemp.equals("A")){textoTemp = "10";} 
if(textoTemp.equals("B")){textoTemp = "11";} 
if(textoTemp.equals("C")){textoTemp = "12";} 
if(textoTemp.equals("D")){textoTemp = "13";} 
if(textoTemp.equals("E")){textoTemp = "14";} 
if(textoTemp.equals("F")){textoTemp = "15";} 
num += Double.parseDouble(textoTemp) * Math.pow(16,j); 
j++; 
} 
texto = df.format(num); 
texto = texto.substring(0,texto.length()-2); 
texto = texto.replace('.', ' '); 
texto = texto.trim(); 
num = 0; 
return texto; 
} 
}
*/