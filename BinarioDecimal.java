package lenguajes.de.intefaz;

/**
 *
 * @author Lucero
 */

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BinarioDecimal {
 public static void main(String[] args) {
 	try{
   UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());            
  }
  catch(Exception e){            
  }
  String leer;
  Pattern patron;
		Matcher match;
		int nro;
		do{
			if((leer = JOptionPane.showInputDialog("Ingresa un número binario:\n"))!= null){
				if((Pattern.matches("^\\s*(-?[0-1]+)\\s*$", leer))){
					patron = Pattern.compile("^\\s*(-?[0-1]+)\\s*$");
     match = patron.matcher(leer);
     match.find();
     leer = match.group(1);
     nro = Integer.parseInt(leer,2);
     break;
				}				
			}else{
				System.exit(0);
			}
			JOptionPane.showMessageDialog(null,"¡Error! Número binario no válido.\nIngresa por ejemplo:  101","Error",JOptionPane.WARNING_MESSAGE);	
		}while(true); 	
 	JOptionPane.showMessageDialog(null,"El numero binario " + leer + "\nEn decimal es: " + nro,"Resultado:",JOptionPane.PLAIN_MESSAGE); 
 }
}
